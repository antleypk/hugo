+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 100
#background = ""

title = "Magyk Word"
#subtitle = "Quality in Delivery"
+++





---


Magyk is a distributed company, based in the United States, with the goal of creating powerful and practical solutions in software, data services, and communication.

Magyk provides websites, databases, and analytics at affordable prices. Magyk Software would like to provide tech support to whatever endeavour you are facing.

If we could be of service to you or your organization please contact us.

**Serving NC and the United States since 2019**