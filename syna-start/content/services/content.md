+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 100
#background = ""

title = "Services Offered"
subtitle = ""



+++



**Magyk Software Develops and Services the Following**

---

<li>Databases
<ul> MySQL <i class="fas fa-database"></i>
<br> Snowflake <i class="far fa-snowflake"></i>
<br> Oracle <i class="fas fa-server"></i>
<br> Neo4j <i class="fa fa-circle"></i>
</ul>
<li>Data Pipelines
<ul>Python <i class="fab fa-python"></i>
<br>Nodes.js <i class="fab fa-node"></i>
</ul>
<li>Applications
<ul> Python <i class="fab fa-python"></i>
<br> Typescript <i class="fab fa-windows"></i>
<br> Angular <i class="fab fa-angular"> </i>
<br> JavaScript <i class="fab fa-js-square"></i>
</ul>
</li>

We are always learning, please ask about any service you would like to purchase.

