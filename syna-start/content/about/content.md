+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 100
#background = ""

title = "About Magyk"
#subtitle = "Quality in Delivery"
+++





---


Magyk Software provides simple solutions for complex problems. We offer a full suite of services (web, application, database, and business management). We are experienced in most modern technologies not limited to: Python, SQL, JavaScript, Wordpress, HTML, CSS, AWS, Databases, UNIX, and Linux. We pride ourselves in customer satisfaction and providing quality dependable work to our clients. Magyk Software can implement new solutions, fix broken applications/machines, consult on design, and contract out development labor.

Magyk Software specializes in optimizing small business processes. Magyk developers are masters of automation, and can provide robust solutions to streamline your business operations.

We are proudly based in Morganton, NC. We enjoy being able to work with you and your business in our Western NC Community.
