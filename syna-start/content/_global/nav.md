+++
fragment = "nav"
#disabled = true
date = "2018-05-17"
weight = 0
#background = ""

[repo_button]
  url = "https://gitlab.com/antleypk"
  text = "Code" 
  icon = "fab fa-gitlab"

# Branding options
[asset]
  image = "magyk_2048.png"
  text = "Magyk"
+++
