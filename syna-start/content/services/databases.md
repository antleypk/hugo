+++
title = "Databases"
fragment = "content"
date = 2020-05-14T11:47:45-04:00

weight = 200

+++

Magyk Software is proud to be qualified to work on relational and non-relational databases. Our engineeers can develop SQL, PL/SQL, cypher, and graphql. 

We have experience in data modeling, database design, database debugging, and database enhancement. 

Magyk Software is skilled at transactional systems and their reporting counter parts. 

Magyk Software has also worked with warehousing and big data storage. 


