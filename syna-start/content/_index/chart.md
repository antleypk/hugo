+++
fragment = "item"

date = "2017-10-04"
weight = 110
background = "secondary"
align = "center"
draft = "false"

title = "Magyk Ecosystem"
#subtitle= "Modern Technologies"

# Subtitle pre and post item
#pre = "Prebuilt Tools"
post = "Modern Technolgies"

[asset]
  image = "ecosystem.png"
+++
