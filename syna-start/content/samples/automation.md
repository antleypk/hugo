+++
fragment = "item"

date = "2017-10-04"
weight = 120
background = "secondary"
align = "left"
draft = false

title = "Automation"
subtitle= "Free Human Capital"

# Subtitle pre and post item
pre = "Automation"
post = "Custom Solutions"

[asset]
  image = "automation.png"
+++

It is so hard to find good people. When you find the people that make your company work well it is best to not waste their time with busy work. There are many ways you can utilize modern technologies to streamline your business processes. When you reduce the busy work your good employees have you allow them to focus on growth and improvement instead of paperwork. 

Here at Magyk Software the Development team designed a program to scrape multiple job boards for the Marketing Team. Now the marketing team gets hundreds of highly qualifed leads per day. At the time of writing this our machine has scraped and scored over two hundred thousand posts from reddit, craiglist, and linkedIn. 

After the scraping was built an addition was made to our in house business adminstation software <a href="portal" class="u-link">Magyk Business</a>  The addition allowed the Marketing team to review a filtered and sorted list and then contact the leads that made sense to our company. 


