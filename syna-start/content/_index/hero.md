+++
fragment = "hero"
#disabled = true
date = "2016-09-07"
weight = 50
background = "light" # can influence the text color
particles = true

title = "Magyk"
subtitle = "Magyk Software"

[header]
  image = "header.jpg"


[asset]
  image = "magyk_2048.png"
  width = "500px" # optional - will default to image width
  #height = "150px" # optional - will default to image height

[[buttons]]
  text = "Services"
  url = "/services/"
  color = "info" # primary, secondary, success, danger, warning, info, light, dark, link - default: primary

[[buttons]]
  text = "Samples"
  url = "/samples/"
  color = "primary"

[[buttons]]
  text = "Business Portal"
  url = "https://portal.magyk.cloud/"
  color = "success"
+++


