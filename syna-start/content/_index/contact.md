+++
fragment = "contact"
#disabled = false
date = "2017-09-10"
weight = 1100
#background = "light"
form_name = "defaultContact"

title = "Contact"
subtitle  = "email us: peterantley@magyk.cloud"

# PostURL can be used with backends such as mailout from caddy
post_url = "https://magyk-api.cloud/contact2" #noFolow
email = "peterantey@magyk.cloud"
button = "Send Button" 
#netlify = false

# Optional google captcha
#[recaptcha]
#  sitekey = ""

[message]
  #success = "" # defaults to theme default
  #error = "" # defaults to theme default

# Only defined fields are shown in contact form
[fields.name]
  text = "Your Name *"
  #error = "" # defaults to theme default

[fields.email]
  text = "Your Email *"
  #error = "" # defaults to theme default

[fields.phone]
  text = "Your Phone *"
  #error = "" # defaults to theme default

[fields.message]
  text = "Your Message *"
  #error = "" # defaults to theme default


+++
