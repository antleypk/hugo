+++
fragment = "item"

date = "2017-10-04"
weight = 110
background = "secondary"
align = "right"
draft = "false"

title = "Private Applications"
subtitle= "Modern Technologies"

# Subtitle pre and post item
pre = "Prebuilt Tools"
post = "Custom Solutions"

[asset]
  image = "portal_login.png"
+++

When storing your information online it is important to hold yourself to high standards. 

At Magyk Software we will provide you with the security that makes sense for your project. We can help with system and account administration.  We are experienced in: SSL, password hashing+salting, private networks, virtual private networks, access logging, and device monitoring. 

We can build analytics, automation, administration, operations, and commcerce solutions for you. We can ensure that only the proper people have access the proper data.



